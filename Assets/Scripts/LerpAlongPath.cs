﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine.Utility;
using Cinemachine;
using System;

public class LerpAlongPath : MonoBehaviour
{
    [SerializeField]
    private CinemachinePath track;
    [SerializeField]
    private int segments;
    [SerializeField]
    private float moveDuration;

    private Vector3[] path;

    // Start is called before the first frame update
    void Start()
    {
        GetPathFromTrack();
        StartCoroutine(MoveProjectileAlongPath(moveDuration));
    }

    // Borrow spline from Cinemachine track for ease of setup
    private void GetPathFromTrack()
    {
        path = new Vector3[segments];

        for(int i = 0; i < segments; i++)
        {
            float percent = (float)i / segments;

            path[i] = track.EvaluatePosition(Mathf.Lerp(track.MinPos, track.MaxPos, percent));
        }
    }

    private IEnumerator MoveProjectileAlongPath(float timeToMove)
    {
        Vector3[] points = path;
        float duration = timeToMove;
        float start = Time.time;
        float elapsed = 0;
        // Time.deltaTime is the duration of a single frame,
        // so this will set position to "after one frame of movement"
        while (elapsed < duration )
        {
            elapsed = Time.time - start;
            float pathPosition = elapsed / duration; // our [0, 1] value
            float indexPosition = pathPosition * points.Length; // position in path

            int left = Mathf.FloorToInt(indexPosition);
            int right = Mathf.CeilToInt(indexPosition);
            if (right >= segments)
            {
                transform.position = path[points.Length - 1];
                break;
            }
            float t = indexPosition - left; // percent between left and right position

            transform.position = Vector3.Lerp(points[left], points[right], t);

            yield return null;
        }
    }
}
